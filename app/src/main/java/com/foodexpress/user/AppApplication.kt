package com.foodexpress.user

import android.content.Context
import androidx.appcompat.app.AppCompatDelegate
import androidx.multidex.MultiDex
import com.foodexpress.user.locale.LocaleAwareApplication
import com.foodexpress.user.network.ApiClient
import com.foodexpress.user.repository.commonrepository
import com.foodexpress.user.utils.Commonmessage
import com.foodexpress.user.utils.SessionManager
import com.foodexpress.user.viewmodel.loginviewmodelfactory
import com.squareup.leakcanary.LeakCanary
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.androidXModule
import org.kodein.di.generic.bind
import org.kodein.di.generic.instance
import org.kodein.di.generic.provider
import org.kodein.di.generic.singleton
import timber.log.Timber
import timber.log.Timber.DebugTree


class AppApplication :  LocaleAwareApplication(), KodeinAware {
    override val kodein: Kodein = Kodein.lazy {
        /**
         * Steps to Use Koein
         * 1-> First implement the KodeinAware interface
         * 2-> Initialize Kodein with Lazy initialization
         * 3-> Initialize all the Classes using bind()
         */
        import(androidXModule(this@AppApplication))


        //We can pass instance() method and Kodein will take care of it
        bind() from singleton { ApiClient() }
        bind() from singleton { SessionManager(instance()) }
        bind() from singleton { Commonmessage(instance()) }
        bind() from singleton { commonrepository(instance(),instance()) }

        //LoginViewModelFactory doesn't should be singleton so we will user provider
        //LoginViewModelFactory needs UserRepository
        bind() from provider { loginviewmodelfactory(instance()) }


        if (BuildConfig.DEBUG) {
            Timber.plant(DebugTree())
        }
        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_FOLLOW_SYSTEM)
    }

    override fun attachBaseContext(base: Context) {
        super.attachBaseContext(base)
        MultiDex.install(this)
    }

    override fun onCreate() {
        super.onCreate()
        if (LeakCanary.isInAnalyzerProcess(this)) {
            // This process is dedicated to LeakCanary for heap analysis.
            // You should not init your app in this process.
            return
        }
        LeakCanary.install(this)
    }
}