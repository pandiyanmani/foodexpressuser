package com.foodexpress.user.viewmodel

import android.view.View
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.foodexpress.user.data.Userlogindetails
import com.foodexpress.user.data.Themeandlanguage
import com.foodexpress.user.data.loginresponse
import com.foodexpress.user.repository.commonrepository
import com.foodexpress.user.utils.Coroutines
import kotlinx.coroutines.Job

class Loginviewmodel(private val repository: commonrepository) : ViewModel() {


    var Emailid = MutableLiveData<String>()
    var Password = MutableLiveData<String>()

    private var displaydialog: MutableLiveData<Themeandlanguage>? = null
    private var userMutableLiveData: MutableLiveData<Userlogindetails>? = null
    private var getcurrentheme: MutableLiveData<String>? = null


    private val _loginpageresponse by lazy {
        MutableLiveData<loginresponse?>()
    }

    val loginpageresponse: MutableLiveData<loginresponse?> get() = _loginpageresponse

    private lateinit var job: Job
    fun getLoginResponseFromRepository(loginUser:Userlogindetails) {
        job = Coroutines.ioToMainThread(
            {
                repository.getLoginresponse(loginUser)
            },
            {
                _loginpageresponse.value = it
            }
        )
    }


    override fun onCleared() {
        super.onCleared()
        if (::job.isInitialized) job.cancel()
    }


    fun displaydialog(): MutableLiveData<Themeandlanguage>? {
        if (displaydialog == null) {
            displaydialog = MutableLiveData()
        }
        return displaydialog
    }

    fun getcurrentthemeobserver(): MutableLiveData<String>? {
        if (getcurrentheme == null) {
            getcurrentheme = MutableLiveData()
        }
        return getcurrentheme
    }

    fun getlanguagecode() = displaydialog!!.postValue(repository.getthemeandlanguage())

    fun gettheme() = getcurrentheme!!.postValue(repository.currenttheme())


    fun getUser(): MutableLiveData<Userlogindetails>? {
        if (userMutableLiveData == null) {
            userMutableLiveData = MutableLiveData()
        }
        return userMutableLiveData
    }

    fun onClick(view: View) {
        val loginUser = Userlogindetails(Emailid.value.toString(), Password.value.toString())
        userMutableLiveData!!.setValue(loginUser)
    }

    fun changelanguageonclick(languagecodes:String)
    {
        repository.changelanguagesettings(languagecodes)
    }

    fun updatecurrenttheme(theme:String)
    {
        repository.updatecurrenttheme(theme)
    }

    fun changelanguagedialogshow(view:View)
    {
        getlanguagecode()
    }

}
