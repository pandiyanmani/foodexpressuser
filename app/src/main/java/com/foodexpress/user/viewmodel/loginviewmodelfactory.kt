package com.foodexpress.user.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.foodexpress.user.repository.commonrepository

@Suppress("UNCHECKED_CAST")
class loginviewmodelfactory(private val moviesRepository: commonrepository) :
    ViewModelProvider.NewInstanceFactory() {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {

        return Loginviewmodel(
            moviesRepository
        ) as T
    }
}