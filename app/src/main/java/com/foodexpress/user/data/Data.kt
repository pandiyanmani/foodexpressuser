package com.foodexpress.user.data

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName



class Data {
    @SerializedName("user_image")
    @Expose
    var userImage: String? = null

    @SerializedName("user_id")
    @Expose
    var userId: String? = null

    @SerializedName("username")
    @Expose
    var username: String? = null

    @SerializedName("email")
    @Expose
    var email: String? = null

    @SerializedName("currency_code")
    @Expose
    var currencyCode: String? = null

    @SerializedName("currency_symbol")
    @Expose
    var currencySymbol: String? = null

    @SerializedName("referal_code")
    @Expose
    var referalCode: String? = null

    @SerializedName("refered_code")
    @Expose
    var referedCode: String? = null

    @SerializedName("location_name")
    @Expose
    var locationName: String? = null

    @SerializedName("country_code")
    @Expose
    var countryCode: String? = null

    @SerializedName("phone_number")
    @Expose
    var phoneNumber: String? = null

    @SerializedName("wallet_amount")
    @Expose
    var walletAmount: Int? = null

    @SerializedName("auth_token")
    @Expose
    var authToken: String? = null

}