package com.foodexpress.user.data

import android.util.Patterns

class Userlogindetails(val email: String, val password: String) {

  val isPasswordLengthGreaterThan5: Boolean
        get() = password.length > 5

    val isEmailempty: Boolean
        get() = email.isNotEmpty() && email != "null"

    val isPasswordempty: Boolean
        get() = password.isNotEmpty() && password != "null"

    fun isEmailValid(): Boolean {
        return Patterns.EMAIL_ADDRESS.matcher(email.trim()).matches()
    }
}