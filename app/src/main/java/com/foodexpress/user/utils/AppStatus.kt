package com.foodexpress.user.utils

import android.content.Context
import android.net.ConnectivityManager


class AppStatus {
    var connectivityManager: ConnectivityManager? = null
    var connected = false
    val isOnline: Boolean
        get() {
            try {
                connectivityManager = context!!
                    .getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
                val networkInfo = connectivityManager!!.activeNetworkInfo
                connected = networkInfo != null && networkInfo.isAvailable &&
                        networkInfo.isConnected
                return connected
            } catch (e: Exception) {
            }
            return connected
        }


    companion object {
        private val instance = AppStatus()
        var context: Context? = null
        fun getInstance(ctx: Context): AppStatus {
            context = ctx.applicationContext
            return instance
        }
    }
}