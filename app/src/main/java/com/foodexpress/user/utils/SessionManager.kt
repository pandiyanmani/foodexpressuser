package com.foodexpress.user.utils

import android.content.Context
import android.content.SharedPreferences


class SessionManager(var _context: Context)
{

    var pref: SharedPreferences
    var editor: SharedPreferences.Editor
    var PRIVATE_MODE = 0


    fun changelanguage(languagecode: String?)
    {
        editor.putString(KEY_LANGUAGECODE, languagecode)
        editor.commit()
    }


    fun updatetheme(theme: String?)
    {
        editor.putString(KEY_CURRENTTHEME, theme)
        editor.commit()
    }

    fun getcurrenttheme(): String {
        return pref.getString(KEY_CURRENTTHEME, "day").toString()
    }

    fun getlanguagecode(): String {
        return pref.getString(KEY_LANGUAGECODE, "en").toString()
    }

    fun clearallvaluefromsession()
    {
        editor.clear()
        editor.commit()
    }

    companion object {
        private const val PREFER_NAME = "Foodexpressuser"
        const val KEY_LANGUAGECODE = "name"
        const val KEY_CURRENTTHEME = "theme"
    }

    init {
        pref = _context.getSharedPreferences(PREFER_NAME, PRIVATE_MODE)
        editor = pref.edit()
    }
}