package com.foodexpress.user.view

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.os.Looper

class splashactivity: Activity() {

    companion object {
        private const val SPLASH_DELAY_TIME: Long = 500
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        Handler(Looper.getMainLooper()).postDelayed({
            this.let {
                // Transfer to your main activity after delay 0.5 second.
                it.startActivity(Intent(this, loginactivity::class.java))
                it.finish()
            }
        }, SPLASH_DELAY_TIME)
    }
}