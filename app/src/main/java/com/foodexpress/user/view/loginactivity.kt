package com.foodexpress.user.view


import android.content.Intent
import android.os.Bundle
import android.text.Spannable
import android.text.SpannableString
import android.text.method.PasswordTransformationMethod
import android.text.style.ForegroundColorSpan
import android.view.View
import androidx.appcompat.app.AppCompatDelegate
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.foodexpress.user.R
import com.foodexpress.user.data.Themeandlanguage
import com.foodexpress.user.data.Userlogindetails
import com.foodexpress.user.databinding.LoginScreenBinding
import com.foodexpress.user.dialogfragment.Bottomsheetdialogkt
import com.foodexpress.user.dialogfragment.MessageDialog
import com.foodexpress.user.locale.Locales
import com.foodexpress.user.utils.ApiException
import com.foodexpress.user.utils.AppStatus
import com.foodexpress.user.utils.NoInternetException
import com.foodexpress.user.viewmodel.Loginviewmodel
import com.foodexpress.user.viewmodel.loginviewmodelfactory
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.android.kodein
import org.kodein.di.generic.instance


class loginactivity() : BaseActivity(), KodeinAware
{
    var flag = false
    var binding: LoginScreenBinding? = null
    lateinit var viewModel: Loginviewmodel
    override val kodein: Kodein by kodein()
    private val factory: loginviewmodelfactory by instance()
    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)
        setUPdatabinding()
        setUPViewModel()
        setUPViewModelObservers()
        binding!!.passwordclick.setBackgroundResource(R.drawable.ic_hide)
        binding!!.passwordclick.setOnClickListener {
            if (!flag)
            {
                binding!!.txtPassword.setTransformationMethod(null)
                binding!!.passwordclick.setBackgroundResource(R.drawable.ic_eye)
                flag=true
                binding!!.txtPassword.setSelection(binding!!.txtPassword.length())
            }
            else
            {
                binding!!.txtPassword.setTransformationMethod(PasswordTransformationMethod())
                binding!!.passwordclick.setBackgroundResource(R.drawable.ic_hide)
                flag=false
                binding!!.txtPassword.setSelection(binding!!.txtPassword.length())
            }
        }
    }
    private fun setUPdatabinding()
    {
        binding = DataBindingUtil.setContentView(this, R.layout.login_screen)
        doubletextspannable()
    }
    fun doubletextspannable()
    {
        val word: Spannable = SpannableString(getString(R.string.do_not_have_an_account))
        word.setSpan(ForegroundColorSpan( ContextCompat.getColor(applicationContext,R.color.lightgrey)), 0, word.length, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
        binding!!.donthave.setText(word)
        val wordTwo: Spannable = SpannableString(" " + getString(R.string.sign_up))
        wordTwo.setSpan(ForegroundColorSpan( ContextCompat.getColor(applicationContext,R.color.red)), 0, wordTwo.length, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
        binding!!.donthave.append(wordTwo)
    }
    private fun setUPViewModel()
    {
        viewModel = ViewModelProvider(this, factory).get(Loginviewmodel::class.java)
        binding!!.lifecycleOwner = this
        binding!!.loginviewmodel = viewModel
    }
    private fun setUPViewModelObservers()
    {
        viewModel.getUser()!!.observe(this,
            Observer<Userlogindetails> { loginUser ->
                if (!loginUser.isEmailempty) {
                    messagedialog(
                        getString(R.string.mandatory),
                        getString(R.string.emailemptycheck)
                    )
                    binding!!.emailid.requestFocus()
                } else if (!loginUser.isEmailValid()) {
                    messagedialog(getString(R.string.mandatory), getString(R.string.emailnotvalid))
                    binding!!.emailid.requestFocus()
                } else if (!loginUser.isPasswordempty) {
                    messagedialog(getString(R.string.mandatory), getString(R.string.enter_password))
                    binding!!.txtPassword.requestFocus()
                } else if (!loginUser.isPasswordLengthGreaterThan5) {
                    messagedialog(
                        getString(R.string.mandatory),
                        getString(R.string.passwordlengthcheck)
                    )
                    binding!!.txtPassword.requestFocus()
                } else {
                    try
                    {
                        if (AppStatus.getInstance(this).isOnline)
                        {
                            if(binding!!.commonloaderpage.visibility != View.VISIBLE)
                            {
                                binding!!.commonloaderpage.visibility = View.VISIBLE
                                viewModel!!.getLoginResponseFromRepository(loginUser)
                            }
                        }
                        else
                            messagedialog(getString(R.string.nointernet),getString(R.string.nointernetmessage))
                    } catch (e: ApiException) {
                    } catch (noInternet: NoInternetException) {
                    }
                }
            })

        viewModel.displaydialog()!!.observe(this,
            Observer<Themeandlanguage> { languagecode ->
                bottomsheetdialog(languagecode)
            })

        viewModel.loginpageresponse.observe(this, Observer { loginresponse ->
            binding!!.commonloaderpage.visibility = View.INVISIBLE
            if(loginresponse!!.status == 0)
                messagedialog(getString(R.string.errormessage),loginresponse!!.message!!)
        })

        viewModel.getcurrentthemeobserver()!!.observe(this,
            Observer<String> { currenttheme ->
                setUPTheme(currenttheme)
            })
        viewModel.gettheme()
    }
    fun setUPTheme(currenttheme:String)
    {
       if (currenttheme.equals("day"))
           AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO)
       else if (currenttheme.equals("night"))
           AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES)
    }
    fun messagedialog(title: String, subtitle: String)
    {
        MessageDialog.newInstanced(title, subtitle).show(supportFragmentManager, "MessageDialog")
    }
    fun bottomsheetdialog(languagecode: Themeandlanguage)
    {
        Bottomsheetdialogkt.newInstancez(languagecode.languagecode,languagecode.theme)
        val fragmentModalBottomSheet = Bottomsheetdialogkt()
        fragmentModalBottomSheet.show(supportFragmentManager, "BottomSheet Fragment")
    }
    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onMessageEvent(event: String?)
    {
        if (event.equals("en"))
        {
            updateLocale(Locales.English)
        }
        else if (event.equals("tr"))
        {
            updateLocale(Locales.Turkish)
        }
        else if (event.equals("ur"))
        {
            updateLocale(Locales.Urdu)
        }
        else if (event.equals("day"))
        {
            viewModel.updatecurrenttheme("day")
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO)
        }
        else if (event.equals("night"))
        {
            viewModel.updatecurrenttheme("night")
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES)
        }
        startAppfromBeggining()
    }
    override fun onStart()
    {
        super.onStart()
        EventBus.getDefault().register(this)
    }
    override fun onStop()
    {
        super.onStop()
        EventBus.getDefault().unregister(this)
    }
    fun startAppfromBeggining()
    {
        val intent = Intent(applicationContext, splashactivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        startActivity(intent)
        finish()
    }
}