package com.foodexpress.user.repository

import com.foodexpress.user.data.Userlogindetails
import com.foodexpress.user.data.Themeandlanguage
import com.foodexpress.user.network.ApiClient
import com.foodexpress.user.network.SafeApiRequest
import com.foodexpress.user.utils.SessionManager


class commonrepository(private val api: ApiClient, private val session: SessionManager) : SafeApiRequest() {


    //Api hit start
    suspend fun getLoginresponse(loginUser:Userlogindetails) = makeApiRequest {
        api.getLoginResponsefromApi(loginUser)
    }


     //Session Manager Update
     fun changelanguagesettings(languagecode:String) {
         session.changelanguage(languagecode)
     }
    fun currenttheme() = session.getcurrenttheme()
    fun getthemeandlanguage(): Themeandlanguage {
        val loginUser = Themeandlanguage(session.getcurrenttheme(),session.getlanguagecode())
        return loginUser
    }
    fun updatecurrenttheme(theme:String) {
        session.updatetheme(theme)
    }
    //End Of Session Manager Update



}