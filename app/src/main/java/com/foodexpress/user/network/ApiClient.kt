package com.foodexpress.user.network


import com.foodexpress.user.BuildConfig
import com.foodexpress.user.data.Userlogindetails
import com.foodexpress.user.data.loginresponse
import com.google.gson.GsonBuilder
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.*


interface ApiClient
{
    companion object {
        //Whenever we will write ApiClient(),it will call invoke() function automatically
        operator fun invoke(): ApiClient {
            val logging = HttpLoggingInterceptor()
            logging.setLevel(HttpLoggingInterceptor.Level.BASIC)

            val gson = GsonBuilder()
                .setLenient()
                .create()

            val client = OkHttpClient.Builder()
                .addInterceptor(logging)
                .build()
            return Retrofit.Builder()
                .client(client)
                .baseUrl(BuildConfig.API_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build()
                .create(ApiClient::class.java)
        }
    }

    @POST("mobile/users/user-login")
    suspend fun getLoginResponsefromApi(@Body login: Userlogindetails?): Response<loginresponse>
}