package com.foodexpress.user.dialogfragment


import android.annotation.SuppressLint
import android.app.Dialog
import android.view.LayoutInflater
import android.view.View
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import com.foodexpress.user.R
import com.foodexpress.user.databinding.BottomsheetdialogBinding
import com.foodexpress.user.viewmodel.Loginviewmodel
import com.foodexpress.user.viewmodel.loginviewmodelfactory
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetBehavior.BottomSheetCallback
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import org.greenrobot.eventbus.EventBus
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.kodein
import org.kodein.di.generic.instance
class Bottomsheetdialogkt : BottomSheetDialogFragment(), KodeinAware {
    private val mBottomSheetBehaviorCallback: BottomSheetCallback = object : BottomSheetCallback() {
        override fun onStateChanged(
            bottomSheet: View,
            newState: Int
        ) {
            when (newState) {
                BottomSheetBehavior.STATE_COLLAPSED -> {
                    run {}
                    run {}
                    run {}
                    run { dismiss() }
                    run {}
                }
                BottomSheetBehavior.STATE_SETTLING -> {
                    run {}
                    run {}
                    run { dismiss() }
                    run {}
                }
                BottomSheetBehavior.STATE_EXPANDED -> {
                    run {}
                    run { dismiss() }
                    run {}
                }
                BottomSheetBehavior.STATE_HIDDEN -> {
                    run { dismiss() }
                    run {}
                }
                BottomSheetBehavior.STATE_DRAGGING -> {
                }
            }
        }

        override fun onSlide(
            bottomSheet: View,
            slideOffset: Float
        ) {
        }
    }
    var binding: BottomsheetdialogBinding? = null
    lateinit var viewModel: Loginviewmodel
    private val factory: loginviewmodelfactory by instance()
    companion object
    {
        private var KEY_CODE = "KEY_CODE"
        private var KEY_THEME = "KEY_THEME"
        fun newInstancez(code: String,theme: String)
        {
            KEY_THEME = theme
            KEY_CODE = code
        }
    }
    @SuppressLint("RestrictedApi")
    override fun setupDialog(dialog: Dialog, style: Int) {
        super.setupDialog(dialog, style)
        setUPdatabinding(dialog)
        setUPViewModel()
        setUPselectedcode()
    }
    //Declaring Databinding with layout behaviour
    private fun setUPdatabinding(dialog: Dialog)
    {
        binding = DataBindingUtil.inflate(
            LayoutInflater.from(context),
            R.layout.bottomsheetdialog, null, false
        )
        dialog.setContentView(binding!!.root)
        val params =
            (binding!!.root.parent as View).layoutParams as CoordinatorLayout.LayoutParams
        val behavior = params.behavior
        if (behavior != null && behavior is BottomSheetBehavior<*>) {
            behavior.setBottomSheetCallback(mBottomSheetBehaviorCallback)
        }
    }
    //Declaring viewmodel with databinding
    private fun setUPViewModel()
    {
        viewModel = ViewModelProvider(this, factory).get(Loginviewmodel::class.java)
        binding!!.english.setOnClickListener {
            updatelanguage("en")
        }
        binding!!.tamil.setOnClickListener {
            updatelanguage("tr")
        }
        binding!!.urdu.setOnClickListener {
            updatelanguage("ur")
        }
        binding!!.btnPositive.setOnClickListener {
            dismiss()
        }
        binding!!.daytheme.setOnClickListener {
            updatetheme("day")
        }
        binding!!.nighttheme.setOnClickListener {
            updatetheme("night")
        }
    }
    fun updatetheme(theme:String)
    {
        dismiss()
        EventBus.getDefault().post(theme)
    }
    fun setUPselectedcode()
    {
        if(KEY_CODE.equals("en")) english()
        else if(KEY_CODE.equals("tr")) tamil()
        else if(KEY_CODE.equals("ur")) urdu()
        if(KEY_THEME.equals("day")) day()
        else if(KEY_THEME.equals("night")) night()
    }
    fun updatelanguage(code:String)
    {
        dismiss()
        viewModel.changelanguageonclick(code)
        EventBus.getDefault().post(code)
    }
    fun day()
    {
        binding!!.datthemetick.visibility = View.VISIBLE
        binding!!.nightthemetick.visibility = View.INVISIBLE
    }
    fun night()
    {
        binding!!.datthemetick.visibility = View.INVISIBLE
        binding!!.nightthemetick.visibility = View.VISIBLE
    }
    fun english()
    {
        binding!!.englishtick.visibility = View.VISIBLE
        binding!!.tamiltick.visibility = View.INVISIBLE
        binding!!.urdhutick.visibility = View.INVISIBLE
    }
    fun tamil()
    {
        binding!!.englishtick.visibility = View.INVISIBLE
        binding!!.tamiltick.visibility = View.VISIBLE
        binding!!.urdhutick.visibility = View.INVISIBLE
    }
    fun urdu()
    {
        binding!!.englishtick.visibility = View.INVISIBLE
        binding!!.tamiltick.visibility = View.INVISIBLE
        binding!!.urdhutick.visibility = View.VISIBLE
    }
    override val kodein: Kodein by kodein()
}