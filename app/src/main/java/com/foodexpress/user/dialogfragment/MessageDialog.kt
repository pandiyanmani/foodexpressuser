package com.foodexpress.user.dialogfragment

import android.app.AlertDialog
import android.app.Dialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.WindowManager
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.DialogFragment
import com.foodexpress.user.R
import com.foodexpress.user.databinding.MessagedialogBinding
class MessageDialog : DialogFragment() {
    companion object
    {
        var binding: MessagedialogBinding? = null
        private const val KEY_TITLE = "KEY_TITLE"
        private const val KEY_SUBTITLE = "KEY_SUBTITLE"

        fun newInstanced(title: String, subTitle: String): MessageDialog
        {
            val args = Bundle()
            args.putString(KEY_TITLE, title)
            args.putString(KEY_SUBTITLE, subTitle)
            val fragment = MessageDialog()
            fragment.arguments = args
            return fragment
        }

    }
    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val activity = activity
         binding = DataBindingUtil.inflate(
            LayoutInflater.from(context),
            R.layout.messagedialog, null, false
        )
        setupView()
        setupClickListeners()
        return AlertDialog.Builder(activity, R.style.Theme_AppCompat_DayNight_Dialog_Alert)
            .setView(binding!!.getRoot())
            .create()
    }
    override fun onStart()
    {
        super.onStart()
        dialog?.window?.setLayout(WindowManager.LayoutParams.MATCH_PARENT,WindowManager.LayoutParams.WRAP_CONTENT)
    }
    private fun setupView()
    {
        binding!!.tvTitle.text = arguments?.getString(KEY_TITLE)
        binding!!.tvSubTitle.text = arguments?.getString(KEY_SUBTITLE)
    }
    private fun setupClickListeners()
    {
        binding!!.btnPositive.setOnClickListener {
            // TODO: Do some task here
            dismiss()
        }
    }
}
